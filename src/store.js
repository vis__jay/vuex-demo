import Vue from 'vue'
import Vuex from 'Vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    //只有mutations中的方法才能修改state中的值
    add (state) {
      //state代表state对象
      state.count++
    },
    addN (state, step) {
      state.count += step
    },
    sub (state) {
      state.count--
    },
    subN (state, step) {
      state.count -= step
    }
  },
  actions: {
    //异步处理
    subAsync (context) {
      setTimeout(() => {
        context.commit('sub')
      }, 1000)
    }
  }
})